using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerEffects : MonoBehaviour
{
    [SerializeField] private ParticleSystem playerEffectIn;
    [SerializeField] private ParticleSystem playerEffectOut;



    private void OnEnable()
    {
        PlayerManager.OnCreate += PlayInEffect;
        PlayerManager.OnDestroy += PlayOutEffect;
    }

    private void OnDisable()
    {
        PlayerManager.OnCreate -= PlayInEffect;
        PlayerManager.OnDestroy -= PlayOutEffect;
    }





    private void PlayInEffect()
    {
        if (transform.root.GetComponent<PlayerManager>().GetPlayerInstant() != null)
        {
            transform.position = transform.root.GetComponent<PlayerManager>().GetPlayerInstant().transform.position;
            playerEffectIn.Play();

        }
    }

    private void PlayOutEffect()
    {
        if (transform.root.GetComponent<PlayerManager>().GetPlayerInstant() != null)
        {
            transform.position = transform.root.GetComponent<PlayerManager>().GetPlayerInstant().transform.position;
            playerEffectIn.Stop();
            playerEffectOut.Play();
            Invoke("StopPlaying", 1);
        }


    }

    private void StopPlaying()
    {
        playerEffectOut.Stop();

    }

}
