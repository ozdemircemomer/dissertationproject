using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;

public class DebugUI : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI RM;
    [SerializeField] private TextMeshProUGUI RBool;
    [SerializeField] private TextMeshProUGUI RT;
    [SerializeField] private TextMeshProUGUI Coins;
    [SerializeField] private TextMeshProUGUI Timer;
    [SerializeField] private GameObject TimerText;



    private bool randomBool;
    private int randomMultiplier;
    private string randomType;
    private float timeLeftForTimer;



    private void Update()
    {
        randomBool = FindAnyObjectByType<PlayerController>().UpdateBoolForRBool();
        randomMultiplier = FindAnyObjectByType<PlayerController>().UpdateBoolForRM();
        randomType = FindAnyObjectByType<PlayerController>().UpdateStringforRT();
        timeLeftForTimer = FindAnyObjectByType<PlayerController>().UpdateTimerText();

        if (randomBool)
        {
            RBool.text = "true";
            TimerText.SetActive(true);

        }
        else
        {
            RBool.text = "false";
            TimerText.SetActive(false);
        }

        RM.text = "x" + randomMultiplier;
        RT.text = randomType;
        Timer.text = "" + timeLeftForTimer;
        Coins.text = SaveData.totalCoin + "";

    }


}
