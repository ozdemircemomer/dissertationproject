using UnityEngine;

public class GameUI : MonoBehaviour
{
    [SerializeField] private GameObject pausePanel;
    private bool pauseState = false; //false = not paused , true = paused


    private void PauseGame()
    {
        if (pauseState)
        {
            Time.timeScale = 1;
            pausePanel.SetActive(false);
            pauseState = false;
        }
        else
        {
            Time.timeScale = 0;
            pausePanel.SetActive(true);
            pauseState = true;
        }
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseGame();

        }

    }

}
