using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallPoint : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Ball"))
        {
            DynamicDiffuculty.teleportOneTime = true;
            PlayerManager.terminateBallAction = true;

        }
    }



}
