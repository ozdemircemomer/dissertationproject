using System.Collections;
using Unity.VisualScripting;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    [Header("Defult Character Controls: ")]
    [SerializeField] float speed;
    [SerializeField] float jumpForce;
    private float horizontal;
    private bool isFacingRight = true;
    [Space(10)]

    [Header("Random Variables: ")]
    //Gives random movement to player.

    [SerializeField] private float timeToRandomise;
    [SerializeField] private int randomizeRangeMin;
    [SerializeField] private int randomizeRangeMax;
    [Space(10)]


    [Header("Random Movement/ Change Possion: ")]
    [SerializeField] private float changePosRangeMin;
    [SerializeField] private float changePosRangeMax;
    [Space(10)]



    [Header("Random Movement/ Change Speed: ")]
    [SerializeField] private float changeSpeedRangeMax;
    [SerializeField] private float changeSpeedRangeMin;
    [Space(10)]


    //Timer Var:
    public float startTime = 5f; // Starting time in seconds
    private float timeRemaining;
    private bool activateTimer = false;


    private int randomizingMultiplier = 1;
    private int randomIndex = 1;
    private float randomLeft;
    private float randomRight;
    private bool randomBool = false;
    private RandomType randomType = RandomType.RandomPossition;
    private string randomTypeName;

    [Header("Control Ref: ")]
    [SerializeField] Camera playerCamera;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundLayer;

    public float RandomChange()
    {

        return Random.Range(randomizeRangeMin, randomizeRangeMax / randomizingMultiplier);
    }

    private void RandomCycleTimer()
    {
        randomIndex = Random.Range(1, 3);
        UpdateRandomCycle();
    }

    private void UpdateRandomCycle()
    {
        switch (randomIndex)
        {
            case 1:
                randomType = RandomType.RandomPossition;
                break;
            case 2:
                randomType = RandomType.RandomSpeed;
                break;
            case 3:
                randomType = RandomType.RandomReverse;
                break;
            case 4:
                randomType = RandomType.Defualt;
                break;
        }

    }


    //Randomly Assigns Random numbers;
    private void CreateRandomMovement()
    {
        if (!randomBool)
        {
            if (RandomChange() == 5)
            {
                randomLeft = Random.Range(-changePosRangeMin, -changePosRangeMax);
                randomRight = Random.Range(changePosRangeMin, changePosRangeMax);
                //Debug.Log("Random Left: " + randomLeft);
                //Debug.Log("Random Right: " + randomRight);
                //StartCoroutine(RandomTime());
                randomBool = true;
                activateTimer = true;
            }
        }
    }

    private void RandomMovementChangePossion()
    {

        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x * GiveRandomX(), jumpForce);

        }

        if (Input.GetButtonUp("Jump") && rb.velocity.y > 0f)
        {
            rb.velocity = new Vector2(rb.velocity.x * GiveRandomX(), rb.velocity.y * 0.5f);
        }

        rb.velocity = new Vector2(horizontal * speed * GiveRandomX(), rb.velocity.y);


    }

    private void RandomMovementChangeSpeed()
    {

        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);

        }

        if (Input.GetButtonUp("Jump") && rb.velocity.y > 0f)
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
        }

        rb.velocity = new Vector2(horizontal * Random.Range(changeSpeedRangeMin, changeSpeedRangeMax), rb.velocity.y);

    }

    private void RandomMovementReverseControls()
    {

        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);

        }

        if (Input.GetButtonUp("Jump") && rb.velocity.y > 0f)
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
        }

        rb.velocity = new Vector2(-horizontal * speed, rb.velocity.y);

    }

    private void Movement()
    {
        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);

        }

        if (Input.GetButtonUp("Jump") && rb.velocity.y > 0f)
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
        }

        rb.velocity = new Vector2(horizontal * speed, rb.velocity.y);

    }


    private float GiveRandomX()
    {

        if (RandomChange() == 6)
        {
            Debug.Log("Changed the Random X Left");
            return randomRight;
        }
        else if (RandomChange() == 7)
        {
            Debug.Log("Changed the Random X Right");

            return randomLeft;

        }
        else
        {
            return 0;

        }

    }




    void Start()
    {
        timeRemaining = startTime;
    }



    void Update()
    {

        CreateRandomMovement();
        UpdateRandomCycle();

        playerCamera.transform.position = new Vector3(transform.position.x, transform.position.y, -1);

        horizontal = Input.GetAxisRaw("Horizontal");

        //Timer
        Timer();

        HandleRandomMovement();

        HandleMovement();

        //Debug 
        if (Input.GetKeyDown(KeyCode.C))
        {

            randomizingMultiplier++;
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {

            randomizingMultiplier = 1;

        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            if (randomIndex != 4)
            {
                randomIndex++;

            }
            else if (randomIndex == 4)
            {
                randomIndex = 1;

            }
        }

        Debug.Log("Current Cycle: " + randomIndex);

        Flip();
    }

    /*
     

        RandomCycleTimer();

     
     */


    //Random Calculation
    private void HandleRandomMovement()
    {

        if (randomBool)
        {
            switch (randomType)
            {
                case RandomType.RandomPossition:
                    RandomMovementChangePossion();
                    randomTypeName = "RandomPossition";
                    break;
                case RandomType.RandomSpeed:
                    RandomMovementChangeSpeed();
                    randomTypeName = "RandomSpeed";

                    break;
                case RandomType.RandomReverse:
                    RandomMovementReverseControls();
                    randomTypeName = "RandomReverse";
                    break;
                case RandomType.Defualt:
                    randomTypeName = "Defualt";
                    break;
            }
        }

    }

    private void HandleMovement()
    {

        if (!randomBool)
        {
            Movement();

        }

    }

    private void Timer()
    {

        if (activateTimer)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                if (timeRemaining < 0)
                {
                    randomBool = false;
                    Debug.Log("Timer: ");
                    timeRemaining = startTime;
                    activateTimer = false;
                }
            }
        }

    }

    private bool IsGrounded()
    {
        return Physics2D.OverlapCircle(groundCheck.position, 0.2f, groundLayer);
    }

    private void Flip()
    {
        if (isFacingRight && horizontal < 0f || !isFacingRight && horizontal > 0f)
        {
            isFacingRight = !isFacingRight;
            Vector3 localScale = transform.localScale;
            localScale.x *= -1f;
            transform.localScale = localScale;
        }
    }





    //Debug Functions:
    public bool UpdateBoolForRBool()
    {
        return randomBool;
    }

    public int UpdateBoolForRM()
    {
        return randomizingMultiplier;
    }

    public string UpdateStringforRT()
    {

        return randomTypeName;
    }

    public float UpdateTimerText()
    {
        return timeRemaining;
    }



}

public enum RandomType
{
    RandomPossition,
    RandomSpeed,
    RandomReverse,
    Defualt
}



/*
 * 
 * 
    IEnumerator RandomTime()
    {
        randomBool = true;
        yield return new WaitForSeconds(timeToRandomise);
        randomBool = false;


    }

 * 
 * 
 private float horizontal;
    private float speed = 8f;
    private float jumpingPower = 16f;
    private bool isFacingRight = true;

    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundLayer;

    void Update()
    {
        horizontal = Input.GetAxisRaw("Horizontal");

        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpingPower);
        }

        if (Input.GetButtonUp("Jump") && rb.velocity.y > 0f)
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
        }

        Flip();
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(horizontal * speed, rb.velocity.y);
    }

    private bool IsGrounded()
    {
        return Physics2D.OverlapCircle(groundCheck.position, 0.2f, groundLayer);
    }

    private void Flip()
    {
        if (isFacingRight && horizontal < 0f || !isFacingRight && horizontal > 0f)
        {
            isFacingRight = !isFacingRight;
            Vector3 localScale = transform.localScale;
            localScale.x *= -1f;
            transform.localScale = localScale;
        }
    }
 
 
 */

