using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using TMPro;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    [SerializeField] private GameObject ball;
    [SerializeField] private GameObject playerPrefab;
    [SerializeField] private Transform startPoint;
    [HideInInspector] public static bool terminateBallAction = false;
    [HideInInspector] public static bool changeToBall = false;
    [HideInInspector] public static bool ballTouchedElectric = false;
    [HideInInspector] public static bool ballTouched = false;
    [SerializeField] private AudioSource teleportSoundEnd;
    [SerializeField] private AudioSource teleportSoundStart;

    public delegate void createEffectAction();
    public static event createEffectAction OnCreate;

    public delegate void destroyEffectAction();
    public static event destroyEffectAction OnDestroy;

    private bool timerActive = false;
    private GameObject playerInstant;
    private GameObject ballInstant;
    private Transform ballPosition;
    private Vector2 teleportPoint;
    private bool ballSpawned = false;
    private bool escapeMenuToggle = false;
    private Vector3 soundPosition = Vector3.zero;

    //Canvas Settings:
    [SerializeField] private TextMeshProUGUI TimerInfo;
    [SerializeField] private GameObject EscapeMenu;




    public void SetBallTouched()
    {

        ballTouched = true;
        Debug.Log("Ball : " + ballTouched);
    }

    public GameObject GetPlayerInstant()
    {
        return playerInstant;
    }

    //Timer Var:
    private float startTime = 3f; // Starting time in seconds
    private float timeRemaining;



    private void Start()
    {
        TimerInfo.text = "" + 0f;

        timeRemaining = startTime;
        playerInstant = Instantiate(playerPrefab, startPoint.position, Quaternion.identity);



    }

    // Update is called once per frame
    void Update()
    {
        HandleEspaceMenu();
        HandleBallSpawn();
        CameraControl();


        if (ballTouchedElectric && !ballTouched)
        {

            Debug.Log("Destroy The Ball");
            PlayerTeleport();
        }
        else if (ballTouched)
        {

            Timer();

        }
    }


    private void CameraControl()
    {

        if (changeToBall)
        {
            if (ballInstant != null)
            {
                Camera.main.transform.position = new Vector3(ballInstant.transform.position.x, ballInstant.transform.position.y, -10);
            }
        }
        else
        {
            if (playerInstant != null)
            {
                Camera.main.transform.position = new Vector3(playerInstant.transform.position.x, playerInstant.transform.position.y, -10);
            }

        }
    }


    private void HandleBallSpawn()
    {

        if (Input.GetMouseButtonDown(0))
        {

            // Debug.Log("pressed R");
            ballPosition = playerInstant.transform.GetChild(0).transform;
            if (ballSpawned)
            {
                if (ballTouched)
                {
                    Debug.Log("Ball Touched a surface cant respawn");
                }
                else
                {
                    ballInstant.GetComponent<Rigidbody2D>().velocity = new Vector3(0f, 0f, 0f);
                    ballInstant.transform.position = ballPosition.position;
                }

            }
            else
            {
                ballInstant = Instantiate(ball, ballPosition.position, Quaternion.identity);
                ballSpawned = true;
            }
        }

    }

    private void HandleEspaceMenu()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (escapeMenuToggle)
            {
                EscapeMenu.SetActive(false);
                Time.timeScale = 1;
                escapeMenuToggle = false;
            }
            else
            {
                EscapeMenu.SetActive(true);
                Time.timeScale = 0;
                escapeMenuToggle = true;
            }
        }
    }


    private void Timer()
    {
        timerActive = true;
        if (timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;
            //Debug.Log("Timer: " + timeRemaining);
            TimerInfo.text = "" + (int)timeRemaining + " Sec";

            if (timeRemaining < 0)
            {
                PlayerTeleport();
                ballTouched = false;
                timeRemaining = startTime;

            }
        }
    }


    private void PlayerTeleport()
    {
        OnCreate();
        //AudioSource.PlayClipAtPoint(teleportSoundStart, transform.position);
        teleportSoundStart.Play();
        ballTouchedElectric = false;
        changeToBall = false;
        //ballInstant.transform.GetChild(0).gameObject.SetActive(true);
        teleportPoint = ballInstant.transform.position;
        Destroy(ballInstant);
        Destroy(playerInstant);
        ballInstant = null;
        StartCoroutine(Wait());



        ballSpawned = false;
        TimerInfo.text = "" + 0f;
        //Debug.Log("Hand Length: " + GetComponent<PlayerHand>().maxRange);
        timerActive = false;
    }


    public void PlayerTeleport(Vector3 teleportPos)
    {
        //ballInstant.transform.GetChild(0).gameObject.SetActive(true);
        teleportPoint = teleportPos;
        //Destroy(ballInstant);
        Destroy(playerInstant);
        // ballInstant = null;
        StartCoroutine(Wait());



        //ballInstant = null;
        // ballSpawned = false;
        // TimerInfo.text = "" + 0f;
        //Debug.Log("Hand Length: " + GetComponent<PlayerHand>().maxRange);

    }





    IEnumerator Wait()
    {
        yield return new WaitForSeconds(1);
        //AudioSource.PlayClipAtPoint(teleportSoundEnd, transform.position);
        teleportSoundEnd.Play();
        playerInstant = Instantiate(playerPrefab, teleportPoint, Quaternion.identity);
        OnDestroy();
        //player.transform.position = teleportPoint;

    }

}

/*
         if (Input.GetKeyDown(KeyCode.E))
        {
            transform.position = ballInstant.transform.position;
            Destroy(ballInstant);
            ballInstant = null;
            ballSpawned = false;
        }

 
 */