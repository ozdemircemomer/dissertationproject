using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHand : MonoBehaviour
{
    [SerializeField] private Transform hammerHead;
    [SerializeField] private Transform body;

    [SerializeField] private float maxRange;
    private float depth;

    // Update is called once per frame
    void FixedUpdate()
    {
        depth = Mathf.Abs(Camera.main.transform.position.z);
        Vector3 center = new Vector3(Screen.width / 2, Screen.height / 2, depth);
        Vector3 mouse = new Vector3(Input.mousePosition.x, Input.mousePosition.y, depth);

        center = Camera.main.ScreenToWorldPoint(center);
        mouse = Camera.main.ScreenToWorldPoint(mouse);

        Vector3 mouseVec = Vector3.ClampMagnitude(mouse - center, maxRange);

        Vector3 newHammerPos = body.position + mouseVec;
        Vector3 hammerMoveVec = newHammerPos - hammerHead.position;
        newHammerPos = hammerHead.position + hammerMoveVec * 0.2f;

        // Update hammer pos
        hammerHead.GetComponent<Rigidbody2D>().MovePosition(newHammerPos);


    }
}


/*



    public void SetBoundries(bool bound)
    {
        inBoundries = bound;
    }

    void Start()
    {

        mainCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

    }
        // Screen center and mouse position in screen space
        // Compute mouseVec for hammer control

        // Update hammer rotation
        //hammerHead.rotation = Quaternion.FromToRotation(Vector3.right, newHammerPos - body.position);
        // Transform to world space


   ContactFilter2D contactFilter = new ContactFilter2D();
        contactFilter.useLayerMask = true;
        contactFilter.layerMask = LayerMask.GetMask("Default");
        Collider2D[] results = new Collider2D[5];


        if (hammerHead.GetComponent<Rigidbody2D>().OverlapCollider(contactFilter, results) > 0)  // If collided with scene objects
        {
            // Update body pos
            Vector3 targetBodyPos = hammerHead.position - mouseVec;

            Vector3 force = (targetBodyPos - body.position) * 80.0f;
            body.GetComponent<Rigidbody2D>().AddForce(force);

            body.GetComponent<Rigidbody2D>().velocity = Vector2.ClampMagnitude(body.GetComponent<Rigidbody2D>().velocity, 6);
        }




    // Update is called once per frane
    void Update()
    {
        mousePos = mainCam.ScreenToWorldPoint(Input.mousePosition);
        Vector2 cubePos = transform.position;

        Vector3 rotation = mousePos;
        rotZ = Mathf.Atan2(-rotation.y, -rotation.x) * Mathf.Rad2Deg;

        //GetComponent<Rigidbody2D>().MovePosition(mousePos);

        GetComponent<Rigidbody2D>().MoveRotation(rotZ);

        if (inBoundries)
        {
           // GetComponent<Rigidbody2D>().MovePosition(mousePos);

        }
        else
        {
           // GetComponent<Rigidbody2D>().MoveRotation(rotZ);

        }



        //transform.position = mousePos;
        //GetComponent<Rigidbody2D>().velocity.
        //transform.rotation = Quaternion.Euler(8, 8, rotZ);

    }

 */