using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MousePoint : MonoBehaviour
{
    private Camera mainCam;
    private Vector2 mousePos;


    void Start()
    {
        mainCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

    }

    // Update is called once per frane
    void Update()
    {
        mousePos = mainCam.ScreenToWorldPoint(Input.mousePosition);
        transform.position  = mousePos;
    }

}
