using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("What is this object: " + collision.gameObject);

        if (collision.CompareTag("Ball"))
        {
            Debug.Log("Ball Passed");

        }

    }
}
