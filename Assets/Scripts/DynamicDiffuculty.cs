using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DynamicDiffuculty : MonoBehaviour
{
    [HideInInspector] public static float ballBouncy;
    [HideInInspector] public static int checkpointIndex;
    [HideInInspector] public static bool negativeEmotionTrigger = false;
    [HideInInspector] public static bool negativeEmotionTrigger2 = false;
    [HideInInspector] public static bool teleportOneTime = true;


    [SerializeField] private GameObject checkpointsGameobject;
    [SerializeField] private PlayerManager playerManager;
    [SerializeField] private TextMeshProUGUI textCheckpointIndex;
    [SerializeField] private TextMeshProUGUI textBouncy;
    [SerializeField] private PhysicsMaterial2D physicsMaterial;
    [SerializeField] private GameObject[] checkpointBridges;
    [SerializeField] private GameObject[] fallPoints;
    [SerializeField] private GameObject[] electricBlocks;
    [SerializeField] private TextMeshProUGUI action;
    [SerializeField] private TextMeshProUGUI totalTimePlayed;


    [SerializeField] private TextMeshProUGUI generalEasyMode;
    [SerializeField] private TextMeshProUGUI bouncinessEasyMode;
    [SerializeField] private TextMeshProUGUI checkpointsEasyMode;

    private int totalCheckpoint;
    private int emotionIndex = 0;
    private bool checkpointsActivated = false;
    private int checkpointProgress = 0;
    private int generalEasyModeID;
    private bool activateOneTimeBounce = true;
    private bool activateOneTimeCheckpoints = true;
    private float totalTime;

    private void Start()
    {
        totalCheckpoint = checkpointsGameobject.transform.childCount;
        checkpointIndex = 0;
        physicsMaterial.bounciness = 0.6f;
    }


    private void Update()
    {
        generalEasyMode.text = "" + generalEasyModeID;
        totalTime += Time.deltaTime;
        totalTimePlayed.text = "" + (int)totalTime;



        if (!activateOneTimeBounce)
        {


        }


        if (!activateOneTimeCheckpoints)
        {

        }




        textCheckpointIndex.text = "" + checkpointIndex;
        textBouncy.text = "" + physicsMaterial.bounciness;
        HandleDebug();
        //HandleDynamicDiffuculty();
        HandleCheckpointsBridges();
        CheckForCheckpointProgress();
    }


    private void HandleCheckpointsBridges()
    {

        if (checkpointsActivated)
        {
            switch (checkpointIndex)
            {
                case 0:
                    Debug.Log("No need to active bridge");
                    checkpointBridges[0].SetActive(false);

                    break;
                case 1:
                    checkpointBridges[0].SetActive(true);
                    //fallPoints[0].SetActive(true);
                    break;
                case 2:
                    checkpointBridges[0].SetActive(true);
                    electricBlocks[0].SetActive(true);
                    //checkpointBridges[1].SetActive(true);
                    //fallPoints[1].SetActive(true);
                    checkpointBridges[2].SetActive(false);


                    break;
                case 3:
                    checkpointBridges[0].SetActive(true);
                    checkpointBridges[1].SetActive(true);
                    electricBlocks[1].SetActive(true);
                    checkpointBridges[2].SetActive(false);

                    // fallPoints[0].SetActive(true);
                    // fallPoints[1].SetActive(true);
                    // fallPoints[2].SetActive(true);

                    //checkpointBridges[2].SetActive(true);

                    break;
                case 4:
                    checkpointBridges[0].SetActive(true);
                    checkpointBridges[1].SetActive(true);
                    checkpointBridges[2].SetActive(true);
                    electricBlocks[2].SetActive(true);

                    // fallPoints[0].SetActive(true);
                    //  fallPoints[1].SetActive(true);
                    //  fallPoints[2].SetActive(true);
                    //  fallPoints[3].SetActive(true);

                    break;
                case 5:
                    checkpointBridges[0].SetActive(true);
                    checkpointBridges[1].SetActive(true);
                    checkpointBridges[2].SetActive(true);
                    checkpointBridges[3].SetActive(true);

                    break;

            }
        }
    }

    private void CheckForCheckpointProgress()
    {
        if (checkpointIndex > checkpointProgress)
        {
            checkpointProgress = checkpointIndex;
        }
    }


    private void HandleDebug()
    {


        if (Input.GetKeyDown(KeyCode.RightArrow))
        {

            Debug.Log("Checkpoint Index is " + checkpointIndex);
            if (checkpointIndex == totalCheckpoint)
            {
                checkpointIndex = 0;

            }
            else
            {
                checkpointIndex++;

            }
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Debug.Log("Checkpoint Index is " + checkpointIndex);

            if (checkpointIndex == 0)
            {
                checkpointIndex = totalCheckpoint;

            }
            else
            {
                checkpointIndex--;

            }
        }


        if (Input.GetKeyDown(KeyCode.T))
        {
            Debug.Log("Checkpoint Index is " + checkpointsGameobject.transform.GetChild(checkpointIndex).transform.position);

            playerManager.PlayerTeleport(checkpointsGameobject.transform.GetChild(checkpointIndex).transform.position);
        }


        if (Input.GetKeyDown(KeyCode.A))
        {
            ActiveReducedBounce();
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            ActiveCheckpoints();
        }





    }



    private void HandleDynamicDiffuculty()
    {

        if (negativeEmotionTrigger)
        {
            Debug.Log("Player is most likely feeling negative emotions");

            if (emotionIndex == 0)
            {
                ActiveReducedBounce();

                emotionIndex = 1;
            }
            else if (emotionIndex == 1)
            {
                ActiveCheckpoints();

            }

            negativeEmotionTrigger = false;
            Debug.Log("negativeEmotionTrigger: " + negativeEmotionTrigger);
        }


    }



    private void OnEnable()
    {
        EmotionArray.OnEmotion += ActiveReducedBounce;
        EmotionArray.OnEmotion2 += ActiveCheckpoints;
    }

    private void OnDisable()
    {
        EmotionArray.OnEmotion -= ActiveReducedBounce;
        EmotionArray.OnEmotion2 -= ActiveCheckpoints;

    }

    private void ActiveReducedBounce()
    {
        if (activateOneTimeBounce)
        {
            action.text = "Decreased ball bounciness";
            physicsMaterial.bounciness = 0.2f;
            generalEasyModeID++;


            bouncinessEasyMode.text = "Active";
            bouncinessEasyMode.color = Color.green;
            activateOneTimeBounce = false;
        }

    }


    private void ActiveCheckpoints()
    {
        action.text = "Decreased ball bounciness" +
            "\n" +
            "Checkpoint Bridges and Electric Blocks Activated";

        checkpointsActivated = true;
        generalEasyModeID++;
        checkpointsEasyMode.text = "Active";
        checkpointsEasyMode.color = Color.green;
        //activateOneTimeCheckpoints = false;


    }


    private void TeleportCheckpoint()
    {
        playerManager.PlayerTeleport(checkpointsGameobject.transform.GetChild(checkpointIndex).transform.position);

    }






}
