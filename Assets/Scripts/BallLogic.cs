using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallLogic : MonoBehaviour
{
    [SerializeField] private AudioSource touchSound;
    [SerializeField] private AudioSource touchSoundElectric;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 6)
        {
            // Debug.Log("Ball Touched ground");
            PlayerManager.ballTouched = true;
            //AudioSource.PlayClipAtPoint(touchSound, soundPosition);
            touchSound.Play();

        }

        if (collision.gameObject.layer == 8)
        {
            // Debug.Log("Ball Touched ground");
            //PlayerManager.ballTouched = true;
            //AudioSource.PlayClipAtPoint(touchSound, soundPosition);
            touchSound.Play();

        }



        if (collision.gameObject.layer == 10)
        {
            // Debug.Log("Ball Touched ground");
            PlayerManager.ballTouchedElectric = true;
            // AudioSource.PlayClipAtPoint(touchSoundElectric, soundPosition);
            touchSoundElectric.Play();
        }

        if (collision.gameObject.layer == 11)
        {
            PlayerManager.changeToBall = true;

        }


    }



}
