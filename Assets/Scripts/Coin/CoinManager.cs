using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinManager : MonoBehaviour
{
    private GameObject[] coins;

    private void Awake()
    {
        coins = new GameObject[transform.childCount];

        for (int i = 0; i < coins.Length; i++)
        {
            coins[i] = transform.GetChild(i).gameObject;

        }
    }


    private void Start()
    {
        foreach (var item in coins)
        {
            Debug.Log("Coin: " + item);
        }
    }

}
