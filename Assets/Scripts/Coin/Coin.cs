using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    private bool collectedCoin = false;
    private GameObject coinModel;

    private void Awake()
    {
        coinModel = transform.GetChild(0).gameObject;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        collectedCoin = true;
        coinModel.SetActive(false);
        GetComponent<BoxCollider2D>().enabled = false;
        SaveData.totalCoin++;
    }


}
