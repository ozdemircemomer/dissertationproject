using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{

    private Scene scene;
    private int sceneIndex;

    private void Awake()
    {
        scene = SceneManager.GetActiveScene();
        sceneIndex = scene.buildIndex;
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        SceneManager.LoadScene(++sceneIndex);

    }


}
