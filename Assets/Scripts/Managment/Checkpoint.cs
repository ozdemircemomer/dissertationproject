using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        SaveData.playerPos = new System.Numerics.Vector3(transform.GetChild(0).position.x, transform.GetChild(0).position.y, collision.transform.position.z);
    }


}
