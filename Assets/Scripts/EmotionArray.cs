using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.UI;

public class EmotionArray : MonoBehaviour
{
    private List<string> emotionArr = new List<string>();
    private List<string> emotionArrBacklog = new List<string>();

    [SerializeField] private Text emotionLog;
    [SerializeField] private FaceRecognizer faceRecognizer;
    [SerializeField] private Text emotionText;
    [SerializeField] private TextMeshProUGUI textAction;
    [SerializeField] private FaceRecognizer emotionDetector;

    public delegate void OnEmotionAction();
    public static event OnEmotionAction OnEmotion;

    public delegate void OnEmotionAction2();
    public static event OnEmotionAction2 OnEmotion2;

    private string currentEmotion;

    private bool activateEasierMode = true;

    void Start()
    {
        InvokeRepeating("GetEmotion", 2.0f, 2f);
        InvokeRepeating("AverageEmotions", 14.0f, 10f);
        InvokeRepeating("HandleEmotionToggle", 20.0f, 10f);

    }

    private void GetEmotion()
    {
        emotionArr.Add(emotionLog.text);
    }

    private void Update()
    {
        HandleEmotionText();
        //HandleEmotionToggle();
        HandleDebug();
        //Debug.Log("Current Emotion: " + emotionDetector.SearchThreshold);
    }


    private void HandleEmotionText()
    {
        if (currentEmotion != null)
        {
            if (currentEmotion.Contains("happy")) { emotionText.text = "The Current Emotion Player is feeling: happy"; }
            else if (currentEmotion.Contains("suprised")) { emotionText.text = "The Current Emotion Player is feeling: suprised"; }
            else if (currentEmotion.Contains("neutral")) { emotionText.text = "The Current Emotion Player is feeling: neutral"; }
            else if (currentEmotion.Contains("scared")) { emotionText.text = "The Current Emotion Player is feeling: scared"; }
            else if (currentEmotion.Contains("angry")) { emotionText.text = "The Current Emotion Player is feeling: angry"; }
            else if (currentEmotion.Contains("sad")) { emotionText.text = "The Current Emotion Player is feeling: sad"; }
            else if (currentEmotion.Contains("disgust")) { emotionText.text = "The Current Emotion Player is feeling: disgust"; }


        }

    }


    private void HandleDebug()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            foreach (var item in emotionArr)
            {
                Debug.Log(item);

            }
        }


        if (Input.GetKeyDown(KeyCode.Z))
        {
            AverageEmotions();
        }


    }



    private void HandleEmotionToggle()
    {

        if (currentEmotion != null)
        {

            if (currentEmotion.Contains("happy") || currentEmotion.Contains("suprised") || currentEmotion.Contains("neutral"))
            {
                textAction.text = "Player is feeling neutral or happy no action needed";

            }
            
            else if (currentEmotion.Contains("scared") || currentEmotion.Contains("angry") || currentEmotion.Contains("sad") || currentEmotion.Contains("disgust"))
            {
                textAction.text = "Player is feeling negative emotions, action needed";

                if (activateEasierMode)
                {
                    OnEmotion();
                    activateEasierMode = false;
                }
                else
                {
                    OnEmotion2();

                }

                //Debug.Log("Player is feeling negative emotions" + testVar++);

                // DynamicDiffuculty.negativeEmotionTrigger = true;
            }

        }


    }






    private void AverageEmotions()
    {
        //CancelInvoke();
        TrimWords();
        currentEmotion = FindMostRecurringWord();
        emotionArr = new List<string>();
    }

    private void TrimWords()
    {
        for (int i = 0; i < emotionArr.Count; i++)
        {
            emotionArr[i] = emotionArr[i].Replace("0", "").Trim();
            emotionArr[i] = emotionArr[i].Replace("Unknown", "").Trim();
            emotionArr[i] = emotionArr[i].Replace(" ", "").Trim();

        }

    }


    private string FindMostRecurringWord()
    {

        // Dictionary to store word counts
        Dictionary<string, int> wordCounts = new Dictionary<string, int>();

        // Count occurrences of each word
        foreach (var word in emotionArr)
        {
            if (wordCounts.ContainsKey(word))
            {
                wordCounts[word]++;
            }
            else
            {
                wordCounts[word] = 1;
            }
        }

        // Find the word with the highest count
        var mostRecurringWord = wordCounts.OrderByDescending(kvp => kvp.Value).FirstOrDefault();

        //Debug.Log("The Emotion Avarage: " + mostRecurringWord);
        return "" + mostRecurringWord;
    }

}


/*
 
 

    public static int MaxOccurrenceOfWord(string[] words)
    {
        var counts = new Dictionary<string, int>();
        int occurrences = 0;
        foreach (var word in words)
        {
            int count;
            counts.TryGetValue(word, out count);
            count++;
            //Automatically replaces the entry if it exists;
            //no need to use 'Contains'
            counts[word] = count;
        }

        string mostCommonWord = null;
        foreach (var pair in counts)
        {
            if (pair.Value > occurrences)
            {
                occurrences = pair.Value;
                mostCommonWord = pair.Key;
            }
        }
        Console.WriteLine("The most common number is {0} and it appears {1} times",
            mostCommonWord, occurrences);

        return occurrences;

    }


 */