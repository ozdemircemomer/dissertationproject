using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EspaceMenu : MonoBehaviour
{

    private bool debugToggle = false;
    [SerializeField] private GameObject debugGameobjects;

    public void DebugButton()
    {
        if (debugToggle)
        {
            debugGameobjects.SetActive(true);
            debugToggle = false;
        }
        else
        {
            debugGameobjects.SetActive(false);
            debugToggle = true;

        }

    }


    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
       // Debug.Log("current Game Scene: " + currentScene);
    }


    public void QuitButton()
    {
        Application.Quit();

    }



}
