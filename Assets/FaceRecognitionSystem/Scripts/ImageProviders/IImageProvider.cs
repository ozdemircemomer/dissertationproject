﻿using UnityEngine;
using UnityEngine.Events;

namespace FaceRecognitionSystem {
    [System.Serializable]
    public class ImageProviderReadyEvent : UnityEvent<IImageProvider> { }
    public interface IImageProvider {
        Color32 [ ] ImgData { get; set; }
        int Width { get; set; }
        int Height { get; set; }
    }
}
