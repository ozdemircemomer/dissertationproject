﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FaceRecognitionSystem;
using System;
using System.Text;
using System.Collections.Concurrent;

[Serializable]
public class Face {
	public string Name;
	public Color BorderColor = Color.green;
	public Texture2D Image;
}

public class FaceRecognizer : MonoBehaviour {
	public List<Face> SearchableFaces = new List<Face>( );
	public List<FaceDescription> FoundFaces = new List<FaceDescription>( );

	public RawImage View = null;
	public float SearchThreshold = 1.2f;
	public int FrameLimit = 10;
	public bool DrawResult = false;

	public bool RecognizeFaces = false;
	public bool DetectGenderAndAge = false;
	public bool DetectEmotion = false;
	public bool GetFaceMesh = false;
	public bool DetectHeadPose = false;

	public Text Log;

	public void OnImageProviderReady( IImageProvider imgProvider ) {
		_imageProvider = imgProvider;
		if ( ( _dt == null ) || ( _dt.width != _imageProvider.Width ) || ( _dt.height != _imageProvider.Height ) ) {
			_dt = new Texture2D( _imageProvider.Width, _imageProvider.Height );
		}
		View.texture = _dt;
		_targetFacesDescs.Clear( );
		if ( RecognizeFaces ) {
			foreach ( var f in SearchableFaces ) {
				var facesLst = _frs.FindFaces( f.Image.GetPixels32( ), f.Image.width, f.Image.height );
				for ( int i = 0; i < facesLst.Count; ++i ) {
					facesLst[ i ].Name = f.Name;
					facesLst[ i ].BorderColor = f.BorderColor;
					_targetFacesDescs.Add( facesLst[ i ] );
				}
			}
		}
		if ( Log != null )
			Log.text = string.Empty;

		_width = imgProvider.Width;
		_height = imgProvider.Height;
	}

	private void Awake( ) {
		_frs = new FRS( );
		_frs.RecognizeFaces = RecognizeFaces;
		_frs.DetectGenderAndAge = DetectGenderAndAge;
		_frs.DetectEmotion = DetectEmotion;
		_frs.GetFaceMesh = GetFaceMesh;
		_frs.DetectHeadPose = DetectHeadPose;

		_frs.log += this.appendLog;
		_frs.SearchThreshold = SearchThreshold;

		_frs.Init( );
	}

	private void Update( ) {
		if ( ( _imageProvider != null ) && ( _imageProvider.ImgData != null ) ) {
			if ( _imageProvider.ImgData.Length > 0 ) {
				if ( _frameCounter >= FrameLimit ) {
					FoundFaces = _frs.FindFaces( _imageProvider.ImgData, _width, _height );
					if ( RecognizeFaces && 
						( _targetFacesDescs.Count > 0 ) && 
						( _targetFacesDescs.Count == SearchableFaces.Count ) && 
						( FoundFaces.Count > 0 ) ) {
						FoundFaces = _frs.FindEqualFaces( _targetFacesDescs, FoundFaces );
					}
					_frameCounter = START_POS;
				}
				else {
					_frameCounter++;
				}
				drawResults( _imageProvider.ImgData, FoundFaces );
				printResults( FoundFaces );
			}
		}
	}

	private void FixedUpdate( ) {

	}

	private void OnDestroy( ) {
		_frs.Dispose( );
	}

    #region Printing result
    private void printResults( List<FaceDescription> results ) {
		if ( Log == null )
			return;
		Log.text = string.Empty;
		if ( results != null ) {
			foreach ( var f in results ) {
				if ( f.Name != string.Empty ) {
					var sb = new StringBuilder( );
					sb.AppendFormat( "<color=#{0}>{1} {2} {3} {4}</color>",
						ColorUtility.ToHtmlStringRGB( f.BorderColor ),
						f.Name,
						f.Gender.ToString( ),
						f.Age,
						f.Emotion.ToString( ) );
					appendLog( sb.ToString( ) );
				}
			}
		}
	}

	private void appendLog( string mess ) {
		if ( Log != null )
			Log.text = mess + "\n" + Log.text;
	}
	#endregion

    #region Drawing result
    private void drawLine( Color32[ ] rawImg, Vector2 p1, Vector2 p2, Color col ) {
		Vector2 t = p1;
		float frac = 1 / Mathf.Sqrt( Mathf.Pow( p2.x - p1.x, 2 ) + Mathf.Pow( p2.y - p1.y, 2 ) );
		float ctr = 0;

		while ( ( int )t.x != ( int )p2.x || ( int )t.y != ( int )p2.y ) {
			t = Vector2.Lerp( p1, p2, ctr );
			ctr += frac;
			markPixelsToColour( rawImg, t, 1, col );
		}
	}

	private void drawResults( Color32[ ] rawImg, List<FaceDescription> results ) {
		const int THICKNESS = 1;
		if ( DrawResult ) {
			if ( results != null ) {
				foreach ( var r in results ) {
					#region draw face rect
					for ( var x = r.Rect.x; x < ( r.Rect.x + r.Rect.width ); ++x ) {
						markPixelsToColour( rawImg, new Vector2( x, r.Rect.y ), THICKNESS, r.BorderColor );
						markPixelsToColour( rawImg, new Vector2( x, r.Rect.y - r.Rect.height ), THICKNESS, r.BorderColor );
					}
					for ( var y = r.Rect.y; y > ( r.Rect.y - r.Rect.height ); --y ) {
						markPixelsToColour( rawImg, new Vector2( r.Rect.x, y ), THICKNESS, r.BorderColor );
						markPixelsToColour( rawImg, new Vector2( r.Rect.x + r.Rect.width, y ), THICKNESS, r.BorderColor );
					}
					#endregion

					#region draw axes
					if ( DetectHeadPose ) {
						var points = r.GetFaceAxisEndpoints( _width, _height );
						var center = new Vector2( r.Rect.x + r.Rect.width / 2, r.Rect.y - r.Rect.height / 2 );
						drawLine( rawImg, center, points[ 0 ], Color.red );
						drawLine( rawImg, center, points[ 1 ], Color.green );
						drawLine( rawImg, center, points[ 2 ], Color.blue );
					}
					#endregion

					#region face mesh
					if ( GetFaceMesh ) {
						foreach ( var p in r.MeshKeypoints ) {
							markPixelsToColour( rawImg, p, THICKNESS, Color.cyan );
						}
					}
					#endregion
				}
			}
		}
		_dt.SetPixels32( rawImg );
		_dt.Apply( );
	}

	private void markPixelsToColour( Color32[ ] rawImg, Vector2 center_pixel, int pen_thickness, Color color_of_pen ) {
		int center_x = ( int )center_pixel.x;
		int center_y = ( int )center_pixel.y;
		for ( int x = center_x - pen_thickness; x <= center_x + pen_thickness; x++ ) {
			if ( x >= ( int )_imageProvider.Width || x < 0 )
				continue;
			for ( int y = center_y - pen_thickness; y <= center_y + pen_thickness; y++ ) {
				markPixelToChange( rawImg, x, y, color_of_pen );
			}
		}
	}

	private void markPixelToChange( Color32[ ] rawImg, int x, int y, Color color ) {
		int array_pos = y * ( int )_imageProvider.Width + x;
		if ( array_pos > rawImg.Length || array_pos < 0 )
			return;
		rawImg[ array_pos ] = color;
	}
    #endregion

	IImageProvider _imageProvider = null;
	private Texture2D _dt = null;
	private List<FaceDescription> _targetFacesDescs = new List<FaceDescription>( );
	private FRS _frs = null;

	private int _width = 0;
	private int _height = 0;

	private const int START_POS = 0;
	private int _frameCounter = START_POS;
}
